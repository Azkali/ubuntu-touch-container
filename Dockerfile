ARG CODENAME 
ARG CI_REGISTRY_IMAGE

FROM ${CI_REGISTRY_IMAGE}:CI-${TARGETARCH}-${CODENAME} as build
ENV DEBIAN_FRONTEND=noninteractive
ONBUILD RUN apt update && apt upgrade -y

# AMD64 Common
FROM build as build_amd64

# ARM64 Common
FROM build as build_arm64
ONBUILD RUN apt install -y \
		gcc-10-multilib-x86-64-linux-gnu \
		gcc-10-multilib-i686-linux-gnu \
		g++-10-multilib-x86-64-linux-gnu \
		g++-10-multilib-i686-linux-gnu \
		g++-10-x86-64-linux-gnu \
		libgcc-10-dev-i386-cross \
		libgcc-10-dev-amd64-cross \
		libstdc++-10-dev-i386-cross \
		libstdc++-10-dev-amd64-cross \
		libstdc++-10-dev-arm64-cross \
		libc6-dev-i386-amd64-cross \
		libc6-dev-amd64-cross \
		libc6-dev-i386-cross

ONBUILD RUN ln -s /usr/bin/x86_64-linux-gnu-gcc-10 /usr/bin/x86_64-linux-gnu-gcc
ONBUILD RUN ln -s /usr/bin/x86_64-linux-gnu-g++-10 /usr/bin/x86_64-linux-gnu-g++

# Focal
FROM build_${TARGETARCH} as build_focal
ONBUILD RUN apt install -y ubports-wallpapers-2018-10 ubports-wallpapers

# Noble
FROM build_${TARGETARCH} as build_noble

# Main
FROM build_${CODENAME}
RUN apt install -y \
		vim \
		git \
		equivs \
		devscripts \
		curl \
		jq \
		cmake \
		ninja-build \
		pkgconf \
		ccache \
		clang \
		llvm \
		lld \
		binfmt-support \
		libsdl2-dev \
		libepoxy-dev \
		libssl-dev \
		nasm \
		python3-clang \
		squashfs-tools \
		squashfuse \
		libc-bin \
		libdrm-dev \
		libxcb-present-dev \
		libxcb-dri2-0-dev \
		libxcb-dri3-dev \
		libxcb-glx0-dev \
		libxcb-shm0-dev \
		libxshmfence-dev \
		libclang-dev \
		libssl-dev \
		xwayland

WORKDIR /root/
CMD /bin/bash
